# API

A API principal é uma API REST feita com o framework [FastAPI](https://fastapi.tiangolo.com/).

É possível acessar o estado atual da API em [http://esther.felipevr.com](http://esther.felipevr.com)

## Rodando Localmente

0. Instale Python >= 3.6
1. Instale o [Poetry](https://python-poetry.org/)
2. Clone esse projeto e instale suas dependências com `poetry install`
3. Rode a API com `poetry run uvicorn api.main:app --reload`

## Usando a API

Depois de executar os comandos acima, a API estará disponível em [http://localhost:8000/](http://localhost:8000/). Uma documentação navegável está disponível em
[http://localhost:8000/docs](http://localhost:8000/docs)

## Autenticação

![login](docs/img/login.gif)

O padrão de autenticação usado é OAuth2 com [JWT](https://jwt.io/). Dado um
usuário com e-mail "test@test.com" e senha "test" ele pode ser logado fazendo um
`POST` do tipo `Form Data` para o endpoint `/token` :

``` sh
    curl -X POST "http://esther.felipevr.com/token" \
         -H  "accept: application/json" \
         -H  "Content-Type: application/x-www-form-urlencoded" \
         -d "grant_type=password&username=test%40test.com&password=test&scope=&client_id=&client_secret="
```

A resposta terá o formato

``` json
{
    "access_token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJ0ZXN0QHRlc3QuY29tIiwiZXhwIjoxNTk1MzQ0NDg2fQ.bMPoVAilnP1J18WLf__OWicuFW2LUKvqJN1QqQnwRog",
    "token_type": "bearer"
}
```

Esse token de acesso é válido por 30min e deve ser passado no cabeçalho
`authorization` dos requests que precisam ser logados

## Rodando Testes

Os testes são feitos com PyTest e Doctest. Podem ser rodados com:

 `poetry run pytest -v --cov=api --cov-report term-missing:skip-covered --doctest-modules`
Esse comando irá rodar os testes do Pytest e do Doctest bem como mostrará um
relatório de cobertura de linhas _apenas_  para os módulos que não possuem
cobertura total

## Hooks Pre-Commit e Pre-Push

Esse projeto também registra hooks do `git` para fazer verificação estática
do código antes do commit e execução de testes antes do push. Para usá-los,
instale o [`pre-commit`](https://pre-commit.com/) e rode `pre-commit install -t pre-commit -t pre-push` .

Se você precisar por algum motivo dar um `commit` ou `push` que está falhando
em algum hook, use a flag `--no-verify` no git. Por exemplo:

 `git commit -m 'WIP - commit quebrado' --no-verify`
 `git push --no-verify`
