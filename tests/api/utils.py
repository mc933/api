"""
Utilitary functions for functional tests
"""
from requests import Response

from api import crud, models, schemas


def create_test_user(
    client, email="test@test.com", password="test", phone="00000", cpf="00000000000"
) -> Response:
    """
    Creates a test user through the HTTP endpoint
    """
    payload = {
        "name": "Test Testson",
        "email": email,
        "password": password,
        "phone": phone,
        "cpf": cpf,
        "push_token": "eee",
    }

    return client.post("/user", json=payload)


def create_test_user_db(
    db, email="test@test.com", password="test", phone="00000", cpf="00000000000"
) -> models.User:
    """
    Creates a test user through the HTTP endpoint
    """
    payload = {
        "name": "Test Testson",
        "email": email,
        "password": password,
        "phone": phone,
        "cpf": cpf,
    }

    return crud.create_user(db, schemas.UserCreate(**payload))


# pylint: disable=too-many-arguments
def creat_test_address(
    db, user: models.User, street="rua test", number="43", city="Test", lat=0, long=0
):
    """
    Util function to create a test address
    """
    address = schemas.AddressCreate(
        street=street, number=number, city=city, lat=lat, long=long
    )
    return crud.create_address(db, address, user)


def log_user_in(client, username="test@test.com", password="test") -> Response:
    """
    Utilitary function that logs an user in the API
    """
    payload = {"username": username, "password": password}

    return client.post("/token", data=payload)


def auth_header(client, username="test@test.com", password="test") -> Response:
    """
    Returns a dict containing the authorization header for a given user
    """
    token = log_user_in(client, username, password).json()["access_token"]
    return {"authorization": f"Bearer {token}"}


def assert_response_matches_payload(
    response, payload, expected_status=200, ignore_keys=None
):
    """
    Helper function to ease checking if a given JSON Response matches an
    input payload that was given to it.

    This function expects that the response's JSON contains at least ALL
    the fields defined on the payload
    """
    assert response.status_code == expected_status

    data = response.json()
    response_keys = set(data.keys())
    ignore_keys = set(ignore_keys) if ignore_keys else set()
    for key, value in payload.items():
        if key in ignore_keys:
            continue
        assert key in response_keys, f"Key '{key}' not present in the response"
        assert data[key] == value, f"Value for '{key}' does not match"


def create_help_request(db, user, **kwargs) -> models.HelpRequest:
    """
    Creates a dummy help request in the database
    """
    help_request = schemas.HelpRequestCreate(**kwargs)
    return crud.create_help_request(db, help_request, user)


def create_help_offer(db, user, **kwargs) -> models.HelpOffer:
    """
    Creates a dummy help offer in the database
    """
    help_offer = schemas.HelpOfferCreate(**kwargs)
    return crud.create_help_offer(db, help_offer, user)
