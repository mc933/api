"""
Functional test for the root of the API
"""


def test_root(client):
    """
    Can we reach the root of the app?
    """
    assert client.get("/").status_code == 200
