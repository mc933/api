"""
Fuctional test for a full help request-asking flow
"""
from fastapi.testclient import TestClient

from api.crud import get_help_offer
from . import utils


def test_offer_help_to_asked_flow(db, client: TestClient):
    """
    Can an asked help be taken?
    """

    user_a = utils.create_test_user_db(
        db, email="userA@test.com", phone="01", cpf="001"
    )
    address_a = utils.creat_test_address(db, user_a)
    auth_a = utils.auth_header(client, user_a.email)

    user_b = utils.create_test_user_db(
        db, email="userB@test.com", phone="02", cpf="002"
    )
    auth_b = utils.auth_header(client, user_b.email)

    help_request_data = {
        "max_cost": 100,
        "notes": "Eu preciso de alguém pra ir ao mercado",
        "help_day": "monday",
        "address_id": address_a.id,
        "min_available_hour": "13",
        "max_available_hour": "20",
    }

    help_request = utils.create_help_request(db, user_a, **help_request_data)

    def user_sees_request(auth):
        response = client.get("/help", headers=auth)
        assert response.status_code == 200
        data = response.json()
        assert len(data) == 1
        assert data[0]["id"] == help_request.id

    user_sees_request(auth_a)
    user_sees_request(auth_b)

    # User B takes A's help request
    payload = {
        "max_cost": 10,
        "notes": "Eu posso ir no mercado!",
        "lat": -22.847_417_8,
        "long": -47.06378,
        "help_day": "monday",
        "min_available_hour": 14,
        "max_available_hour": 17,
    }

    response = client.put(
        f"/help/{help_request.id}/offer", headers=auth_b, json=payload
    )

    assert response.status_code == 200
    data = response.json()
    assert data["id"] == help_request.id
    assert len(data["offers"]) == 1

    offer = data["offers"][0]

    assert offer["id"]
    assert offer["notes"] == payload["notes"]

    # User B tries to mark A's request as fulfilled, but can't
    response = client.put(f"/help/{help_request.id}/fulfill", headers=auth_b)
    assert response.status_code == 403

    # User A marks its own request as fulfilled
    response = client.put(f"/help/{help_request.id}/fulfill", headers=auth_a)
    assert response.status_code == 200

    data = response.json()
    assert isinstance(data["fulfilled"], bool)
    assert data["fulfilled"]


def test_ask_help_to_offer_flow(db, client: TestClient):
    """
    Can an offered help be taken?
    """

    user_a = utils.create_test_user_db(
        db, email="userA@test.com", phone="01", cpf="001"
    )
    auth_a = utils.auth_header(client, user_a.email)

    user_b = utils.create_test_user_db(
        db, email="userB@test.com", phone="02", cpf="002"
    )
    address_b = utils.creat_test_address(db, user_b)
    auth_b = utils.auth_header(client, user_b.email)

    help_offer_data = {
        "max_cost": 300,
        "notes": "Eu vou no mercado, alguém quer algo?",
        "lat": -22.847_417_8,
        "long": -47.06378,
        "help_day": "monday",
        "min_available_hour": 14,
        "max_available_hour": 17,
    }

    help_offer = utils.create_help_offer(db, user_a, **help_offer_data)

    def user_sees_offer(auth):
        response = client.get("/help_offer", headers=auth)
        assert response.status_code == 200
        data = response.json()
        assert len(data) == 1
        assert data[0]["id"] == help_offer.id

    user_sees_offer(auth_a)
    user_sees_offer(auth_b)

    help_request_data = {
        "max_cost": 100,
        "notes": "Eu preciso de alguém pra ir ao mercado",
        "help_day": "monday",
        "address_id": address_b.id,
        "min_available_hour": "13",
        "max_available_hour": "20",
        "items": [{"name": "banana", "quantity": 1, "max_cost": 10}],
    }

    response = client.put(
        f"/help_offer/{help_offer.id}/ask", headers=auth_b, json=help_request_data
    )

    assert response.status_code == 200
    data = response.json()
    assert data["id"] == help_offer.id
    help_db = get_help_offer(db, data["id"])
    assert len(help_db.requests) == 1

    request = help_db.requests[0]

    assert request.notes == help_request_data["notes"]

    response = client.get(f"/help/{request.id}", headers=auth_b)
    assert response.status_code == 200
    assert "items" in response.json()

    # User B tries to mark A's offer as finished, but can't
    response = client.put(f"/help_offer/{help_offer.id}/finish", headers=auth_b)
    assert response.status_code == 403

    # User A marks its own offer as finished
    response = client.put(f"/help_offer/{help_offer.id}/finish", headers=auth_a)
    assert response.status_code == 200

    data = response.json()
    assert isinstance(data["finished"], bool)
    assert data["finished"]
