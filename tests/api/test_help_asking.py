"""
Functional tests for the process of asking help
"""
from api.crud import get_help_request, get_help_requests
from .utils import (
    assert_response_matches_payload,
    auth_header,
    create_test_user_db,
    creat_test_address,
)


def test_create_help(client, db):
    """
    Tests if a logged in user can create a help request
    """

    # Creating a new Help Request
    user = create_test_user_db(db)
    address = creat_test_address(db, user)
    payload = {
        "max_cost": 100,
        "notes": "test\ntest\ntesttesttesttest",
        "address_id": address.id,
        "help_day": "monday",
        "min_available_hour": "13h",
        "max_available_hour": "20h",
        "type": "test!!",
        "items": [{"name": "banana", "quantity": 1, "max_cost": 10}],
    }

    response = client.post("/help", json=payload, headers=auth_header(client))

    data = response.json()

    assert "address" in data

    assert_response_matches_payload(response, payload, ignore_keys={"items"})

    # Checking the database status
    assert len(get_help_requests(db)) == 1
    help_db = get_help_request(db, data["id"])

    assert help_db.asking_user_id == 1
    assert help_db.notes == payload["notes"]
    assert help_db.help_day == payload["help_day"]
    assert help_db.min_available_hour == payload["min_available_hour"]
    assert help_db.max_available_hour == payload["max_available_hour"]
    assert help_db.address
    assert help_db.address.street == address.street

    assert not help_db.fulfilled

    # The new help request should appear on the help request listing
    response = client.get("/help", json=payload, headers=auth_header(client))
    assert response.status_code == 200
    assert len(response.json()) == 1
    assert response.json()[0] == data

    # The new help request should also appear in the user data
    response = client.get("/me", json=payload, headers=auth_header(client))

    assert response.status_code == 200
    user_data = response.json()
    assert "asked_helps" in user_data
    asked_helps = user_data["asked_helps"]
    assert len(asked_helps) == 1
    assert asked_helps[0] == data
