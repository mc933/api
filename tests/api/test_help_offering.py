"""
Functional tests for the process of offering help
"""
from api.crud import get_help_offer, get_help_offers, get_user
from .utils import assert_response_matches_payload, auth_header, create_test_user


def test_create_help_offer(client, db):
    """
    Tests if a logged in user can create a help offer
    """

    # Creating a new Help Offer
    user_data = create_test_user(client).json()
    user = get_user(db, user_data["id"])
    payload = {
        "max_cost": 100,
        "notes": "test\ntest\ntesttesttesttest",
        "lat": -22.847_417_8,
        "long": -47.06378,
        "help_day": "monday",
        "hour": "10h",
        "type": "test!",
    }

    response = client.post("/help_offer", json=payload, headers=auth_header(client))

    data = response.json()

    assert_response_matches_payload(response, payload)

    # Checking the database status
    assert len(get_help_offers(db)) == 1
    help_db = get_help_offer(db, data["id"])

    assert help_db.offering_user_id == 1
    assert help_db.notes == payload["notes"]
    assert help_db.lat == payload["lat"]
    assert help_db.long == payload["long"]
    assert help_db.help_day == payload["help_day"]
    assert help_db.hour == payload["hour"]

    assert len(user.offered_helps) == 1
    assert user.offered_helps[0] == help_db
    assert help_db.offering_user == user

    response = client.get("/help_offer", json=payload, headers=auth_header(client))
    assert response.status_code == 200
    assert len(response.json()) == 1
    assert response.json()[0] == data

    # The new help request should also appear in the user data
    response = client.get("/me", json=payload, headers=auth_header(client))

    assert response.status_code == 200
    user_data = response.json()
    assert "offered_helps" in user_data
    offered_helps = user_data["offered_helps"]
    assert len(offered_helps) == 1
    assert offered_helps[0] == data
