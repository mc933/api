"""
Fuctional test for adding items to a request
"""
from fastapi.testclient import TestClient

from . import utils


def test_request_items(db, client: TestClient):
    """
    Can an asked help have items attached to it
    """

    user_a = utils.create_test_user_db(
        db, email="userA@test.com", phone="01", cpf="001"
    )
    auth_a = utils.auth_header(client, user_a.email)
    address_a = utils.creat_test_address(db, user_a)

    user_b = utils.create_test_user_db(
        db, email="userB@test.com", phone="02", cpf="002"
    )
    auth_b = utils.auth_header(client, user_b.email)

    help_request_data = {
        "max_cost": 100,
        "notes": "Eu preciso de alguém pra ir ao mercado",
        "help_day": "monday",
        "address_id": address_a.id,
        "min_available_hour": "13",
        "max_available_hour": "20",
    }

    help_request = utils.create_help_request(db, user_a, **help_request_data)

    items = [
        {"name": "chocolate", "quantity": 2, "max_cost": 10},
        {"name": "maçã", "quantity": 3, "max_cost": 5},
    ]
    response = client.put(f"/help/{help_request.id}/items", headers=auth_b, json=items)
    assert response.status_code == 403
    response = client.put(f"/help/{help_request.id}/items", headers=auth_a, json=items)
    assert response.status_code == 200

    data = response.json()

    utils.assert_response_matches_payload(response, help_request_data)

    assert "items" in data
    items = data["items"]

    assert len(items) == 2

    # User may delete an item
    response = client.delete(
        f"/help/{help_request.id}/items/{items[0]['id']}", headers=auth_b
    )
    assert response.status_code == 403
    response = client.delete(
        f"/help/{help_request.id}/items/{items[0]['id']}", headers=auth_a
    )
    assert response.status_code == 200
    data = response.json()
    new_items = data["items"]

    assert len(new_items) == 1

    new_item = {"name": "maçã", "quantity": 30, "max_cost": 5}
    # User may update an item
    response = client.put(
        f"/help/{help_request.id}/items/{items[1]['id']}", headers=auth_b, json=new_item
    )
    assert response.status_code == 403
    response = client.put(
        f"/help/{help_request.id}/items/{items[1]['id']}", headers=auth_a, json=new_item
    )

    assert response.json()["items"][0]["quantity"] == new_item["quantity"]
