"""
Functional tests for authentication-related endpoints
"""
from datetime import timedelta
from jose import jwt
from api import auth, crud
from .utils import auth_header, create_test_user, log_user_in, create_test_user_db


def test_create_update_user(client, db):
    """
    Can we create a new user?
    """
    response = create_test_user(client)

    assert response.status_code == 200

    data = response.json()

    assert data["email"] == "test@test.com"
    assert "push_token" in data

    db_user = crud.get_user_by_email_or_phone(db, data["email"])

    assert db_user.id == data["id"]
    assert auth.verify_password("test", db_user.hashed_password)

    get_user = client.get(f"/user/{data['id']}")
    assert get_user.status_code == 200

    assert "push_token" in get_user.json()

    auth_headers = auth_header(client)
    new_data = {
        "name": "New test",
        "email": db_user.email,
        "password": "new password!",
        "phone": "12310",
        "cpf": "99999",
    }

    response = client.put(f"/user/{db_user.id}", headers=auth_headers, json=new_data)
    assert response.status_code == 200
    data = response.json()

    assert data["name"] == new_data["name"]
    assert data["email"] == new_data["email"]
    db_user = crud.get_user_by_email_or_phone(db, data["email"])
    assert auth.verify_password(new_data["password"], db_user.hashed_password)


def test_duplicated_creation_fails(client):
    """
    Create two users with the same email or phone should not work
    """

    # Duplicated email
    create_test_user(client, email="test@test", phone="0000")
    response = create_test_user(client, email="test@test", phone="1111")

    assert response.status_code == 400

    # Duplicated phone
    response_2 = create_test_user(client, email="test22@test222", phone="0000")
    assert response_2.status_code == 400


def test_auth_works(client):
    """
    Can we log an user in?
    """
    create_test_user(client)
    response = log_user_in(client)

    data = response.json()
    assert response.status_code == 200
    assert "access_token" in data

    token = data["access_token"]
    claims = jwt.get_unverified_claims(token)
    assert "sub" in claims
    assert claims["sub"] == "test@test.com"

    response = log_user_in(client, password="invalid")

    assert response.status_code == 401
    assert "access_token" not in response.json()


def test_me_works(client):
    """
    Tests if the /me endpoint identifies the current user
    """
    create_test_user(client)
    header = auth_header(client)
    response = client.get("/me", headers=header)

    assert response.status_code == 200

    data = response.json()

    assert data["email"] == "test@test.com"
    assert data["cpf"] == "00000000000"


def test_cpf_validated(client):
    """
    Tests if CPF is cleaned
    """
    create_test_user(client, cpf="123.456.789-00")
    header = auth_header(client)
    response = client.get("/me", headers=header)

    assert response.status_code == 200

    data = response.json()

    assert data["email"] == "test@test.com"
    assert data["cpf"] == "12345678900"


def test_expired_token_fails(client, db):
    """
    If the user has an invalid token, it should
    get a 401 error on authenticated routes
    """
    user = create_test_user_db(db)
    token = auth.create_access_token(
        data={"sub": user.email}, expires_delta=timedelta(seconds=-10)
    )

    header = {"authorization": f"Bearer {token}"}

    response = client.get("/me", headers=header)

    assert response.status_code == 401
