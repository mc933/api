"""
Functional tests for the process of asking help
"""
from api.crud import get_addresses, get_address, get_user

from .utils import assert_response_matches_payload, auth_header, create_test_user


def test_create_address(client, db):
    """
    Tests if a logged in user can create an address
    """

    # Creating a Address
    user_id = create_test_user(client).json()["id"]
    payload = {
        "street": "Rua Teste",
        "number": "42",
        "city": "Test",
        "lat": 10.123,
        "long": 20.456,
    }

    response = client.post("/address", json=payload, headers=auth_header(client))

    data = response.json()

    assert_response_matches_payload(response, payload)

    # Checking the database status
    assert len(get_addresses(db)) == 1
    address = get_address(db, data["id"])

    assert address.user_id == user_id

    db_user = get_user(db, user_id)

    assert len(db_user.addresses) == 1
    assert address in db_user.addresses

    response = client.get("/me", headers=auth_header(client))

    data = response.json()

    assert "addresses" in data

    addresses = data["addresses"]

    assert len(addresses) == 1
    assert addresses[0]["street"] == payload["street"]

    # Test address deletion
    response = client.delete(f"/address/{address.id}")
    assert response.status_code == 401

    response = client.delete(f"/address/{address.id}", headers=auth_header(client))
    assert response.status_code == 200
