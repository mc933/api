"""
Routes for address handling
"""
from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm.session import Session

from .. import crud, models, schemas
from ..deps import get_current_active_user, get_db

router = APIRouter()


@router.post("/address", response_model=schemas.Address)
def create_address(
    address: schemas.AddressCreate,
    current_user: models.User = Depends(get_current_active_user),
    db: Session = Depends(get_db),
):
    """
    Creates a new address
    """
    return crud.create_address(db, address, current_user)


@router.delete("/address/{address_id}")
def delete_address(
    address_id: int,
    current_user: models.User = Depends(get_current_active_user),
    db: Session = Depends(get_db),
):
    """
    Creates a new address
    """
    address = crud.get_address(db, address_id)
    if not address:
        raise HTTPException(404)

    if address.address_owner != current_user:
        raise HTTPException(401)

    db.delete(address)
    db.commit()

    return 200
