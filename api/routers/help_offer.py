"""
Routes for help offer handling
"""
from typing import List
from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm.session import Session

from .. import crud, models, schemas
from ..deps import get_current_active_user, get_db

import firebase_admin
from firebase_admin import credentials
from firebase_admin import db

cred = credentials.Certificate('/service-account.json')

firebase_admin.initialize_app(cred, {
    'databaseURL': 'https://mc933-5f3df.firebaseio.com'
})

ref = db.reference('notification/new_request_to_offer')
refFinished = db.reference('notification/finished')


router = APIRouter()

@router.post("/help_offer", response_model=schemas.HelpOffer)
def create_help_offer(
    help_offer: schemas.HelpOfferCreate,
    current_user: models.User = Depends(get_current_active_user),
    db: Session = Depends(get_db),
):
    """
    Creates a new help offer
    """
    return crud.create_help_offer(db, help_offer, user=current_user)


@router.get("/help_offer", response_model=List[schemas.HelpOffer])
def read_help_requests(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    """
    Lists all help requests
    """
    return crud.get_help_offers(db, skip=skip, limit=limit)


@router.get("/help_offer/{offer_id}", response_model=schemas.HelpOffer)
def read_help_request(offer_id: int, db: Session = Depends(get_db)):
    """
    Gets a single help request
    """
    return crud.get_help_offer(db, offer_id)


@router.put("/help_offer/{offer_id}/ask", response_model=schemas.HelpOffer)
def offer_help(
    offer_id: int,
    help_request: schemas.HelpRequestCreate,
    current_user: models.User = Depends(get_current_active_user),
    db: Session = Depends(get_db),
):
    """
    Creates an request for a given offer
    """
    help_offer = crud.get_help_offer(db, offer_id)

    if not help_offer:
        raise HTTPException(404, "Help request not found")

    ref.set({
        "user": current_user
    })

    return crud.accept_help_offer(db, help_offer, help_request, current_user)


@router.put("/help_offer/{offer_id}/finish", response_model=schemas.HelpOffer)
def mark_finished(
    offer_id: int,
    current_user: models.User = Depends(get_current_active_user),
    db: Session = Depends(get_db),
):
    """
    Mark a help request as fulfilled
    """
    help_offer = crud.get_help_offer(db, offer_id)

    if not help_offer:
        raise HTTPException(404, "Help request not found")

    if help_offer.offering_user != current_user:
        raise HTTPException(403)

    help_offer.finished = True
    db.add(help_offer)
    db.commit()
    db.refresh(help_offer)

    refFinished.set({
        "user": current_user
    })

    return help_offer
