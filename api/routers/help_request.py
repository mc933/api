"""
Routes for help request handling
"""
from typing import List
from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm.session import Session

from .. import crud, models, schemas
from ..deps import get_current_active_user, get_db

import firebase_admin
from firebase_admin import credentials
from firebase_admin import db

cred = credentials.Certificate('/service-account.json')

firebase_admin.initialize_app(cred, {
    'databaseURL': 'https://mc933-5f3df.firebaseio.com'
})

ref = db.reference('notification/new_offer_to_request')
refFulfilled = db.reference('notification/fulfilled')

router = APIRouter()


@router.post("/help", response_model=schemas.HelpRequest)
def create_help_request(
    help_request: schemas.HelpRequestCreate,
    current_user: models.User = Depends(get_current_active_user),
    db: Session = Depends(get_db),
):
    """
    Creates a new help request
    """
    return crud.create_help_request(db, help_request, user=current_user)


@router.get("/help", response_model=List[schemas.HelpRequest])
def read_help_requests(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    """
    Lists all help requests
    """
    return crud.get_help_requests(db, skip=skip, limit=limit)


@router.get("/help/{request_id}", response_model=schemas.HelpRequestPlain)
def read_help_request(request_id: int, db: Session = Depends(get_db)):
    """
    Lists all help requests
    """
    return crud.get_help_request(db, request_id)


@router.put("/help/{request_id}/offer", response_model=schemas.HelpRequest)
def offer_help(
    request_id: int,
    help_offer: schemas.HelpOfferCreate,
    current_user: models.User = Depends(get_current_active_user),
    db: Session = Depends(get_db),
):
    """
    Creates an offer for a given request
    """
    help_request = crud.get_help_request(db, request_id)

    if not help_request:
        raise HTTPException(404, "Help request not found")

    ref.set({
        "user": current_user
    })

    return crud.offer_help_to_request(db, help_request, help_offer, current_user)


@router.put("/help/{request_id}/fulfill", response_model=schemas.HelpRequest)
def mark_fulfilled(
    request_id: int,
    current_user: models.User = Depends(get_current_active_user),
    db: Session = Depends(get_db),
):
    """
    Mark a help request as fulfilled
    """
    help_request = crud.get_help_request(db, request_id)

    if not help_request:
        raise HTTPException(404, "Help request not found")

    if help_request.asking_user != current_user:
        raise HTTPException(403)

    help_request.fulfilled = True
    db.add(help_request)
    db.commit()
    db.refresh(help_request)

    refFulfilled.set({
        "user": current_user
    })

    return help_request


@router.put("/help/{request_id}/items", response_model=schemas.HelpRequest)
def add_items(
    request_id: int,
    items: List[schemas.ItemCreate],
    current_user: models.User = Depends(get_current_active_user),
    db: Session = Depends(get_db),
):
    """
    Add items to a help request
    """
    help_request = crud.get_help_request(db, request_id)

    if not help_request:
        raise HTTPException(404, "Help request not found")

    if help_request.asking_user != current_user:
        raise HTTPException(403)

    crud.create_items(db, help_request, items)
    db.refresh(help_request)

    return help_request


@router.delete("/help/{request_id}/items/{item_id}", response_model=schemas.HelpRequest)
def remove_item(
    request_id: int,
    item_id: int,
    current_user: models.User = Depends(get_current_active_user),
    db: Session = Depends(get_db),
):
    """
    Remove a help request item
    """
    help_request = crud.get_help_request(db, request_id)
    item = crud.get_help_request_item(db, item_id)

    if not help_request:
        raise HTTPException(404, "Help request not found")

    if help_request.asking_user != current_user:
        raise HTTPException(403)

    db.delete(item)
    db.commit()
    db.refresh(help_request)

    return help_request


@router.put("/help/{request_id}/items/{item_id}", response_model=schemas.HelpRequest)
def update_item(
    request_id: int,
    item_id: int,
    item_data: schemas.ItemCreate,
    current_user: models.User = Depends(get_current_active_user),
    db: Session = Depends(get_db),
):
    """
    Remove a help request item
    """
    help_request = crud.get_help_request(db, request_id)
    item = crud.get_help_request_item(db, item_id)

    if not help_request:
        raise HTTPException(404, "Help request not found")

    if help_request.asking_user != current_user:
        raise HTTPException(403)

    db.query(models.Item).filter(models.Item.id == item.id).update(item_data.dict())
    db.commit()
    db.refresh(help_request)

    return help_request
