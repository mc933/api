"""
Routes for address handling
"""
from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.orm.session import Session

from .. import crud, schemas, models
from ..deps import get_current_active_user, get_db

router = APIRouter()


@router.post("/user", response_model=schemas.User)
def create_user(user: schemas.UserCreate, db: Session = Depends(get_db)):
    """
    Creates a new user
    """
    if crud.get_user_by_email_or_phone(db, user.email, user.phone):
        raise HTTPException(status_code=400, detail="Email already registered")

    return crud.create_user(db, user)


@router.get("/me", response_model=schemas.User)
def check_user(current_user: str = Depends(get_current_active_user)):
    """
    Returns data on the current user
    """
    return current_user


@router.get("/user/{user_id}", response_model=schemas.UserPublic)
def read_user(user_id: int, db: Session = Depends(get_db)):
    """
    Returns public data about an user
    """
    return db.query(models.User).get(user_id)


@router.put("/user/{user_id}", response_model=schemas.User)
def update_user(
    user_id: int,
    user_data: schemas.UserCreate,
    db: Session = Depends(get_db),
    current_user: models.User = Depends(get_current_active_user),
):
    """
    Updates an user
    """
    if current_user.id != user_id:
        raise HTTPException(401)

    conflicting_user = crud.get_user_by_email_or_phone(
        db, user_data.email, user_data.phone
    )

    if conflicting_user and conflicting_user != current_user:
        raise HTTPException(status_code=400, detail="Email already registered")

    return crud.update_user(db, user_id, user_data)
