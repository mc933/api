"""
Help Request SQL Modelling
"""
from sqlalchemy import Column, ForeignKey, Integer, Numeric, Text, Boolean
from sqlalchemy.orm import relationship

from ..database import Base
from .mixins import TimestampMixin


# pylint: disable=too-few-public-methods
class HelpRequest(Base, TimestampMixin):
    """
    Defines the SQLAlchemy model for a Help Request
    """

    __tablename__ = "help_requests"

    id = Column(Integer, primary_key=True, index=True)
    max_cost = Column(Numeric(5, 2), nullable=False)
    notes = Column(Text, nullable=True)
    asking_user_id = Column(Integer, ForeignKey("users.id"))
    help_day = Column(Text, nullable=False)
    min_available_hour = Column(Text)
    max_available_hour = Column(Text)
    fulfilled = Column(Boolean, default=False)
    type = Column(Text, nullable=True)
    address_id = Column(Integer, ForeignKey("addresses.id"))

    asking_user = relationship("User", back_populates="asked_helps")
    offers = relationship(
        "HelpOffer", back_populates="requests", secondary="requests_offers"
    )
    items = relationship("Item")
    address = relationship("Address")
