"""
Help Offer SQL Modelling
"""
from sqlalchemy import Column, Float, ForeignKey, Integer, Numeric, Text, Boolean
from sqlalchemy.orm import relationship

from ..database import Base
from .mixins import TimestampMixin


# pylint: disable=too-few-public-methods
class HelpOffer(Base, TimestampMixin):
    """
    Defines the SQLAlchemy model for a Help Offer
    """

    __tablename__ = "help_offers"

    id = Column(Integer, primary_key=True, index=True)
    max_cost = Column(Numeric(5, 2), nullable=False)
    notes = Column(Text, nullable=True)
    offering_user_id = Column(Integer, ForeignKey("users.id"))
    lat = Column(Float, default=0)
    long = Column(Float, default=0)
    help_day = Column(Text, nullable=False)
    hour = Column(Text)
    finished = Column(Boolean, default=False)
    type = Column(Text, nullable=False)

    offering_user = relationship("User", back_populates="offered_helps")
    requests = relationship(
        "HelpRequest", back_populates="offers", secondary="requests_offers"
    )
