"""
Address SQL Modelling
"""
from sqlalchemy import Column, ForeignKey, Integer, String, Float
from sqlalchemy.orm import relationship

from ..database import Base
from .mixins import TimestampMixin


# pylint: disable=too-few-public-methods
class Address(Base, TimestampMixin):
    """
    Defines the SQLAlchemy model for an Address
    """

    __tablename__ = "addresses"

    id = Column(Integer, primary_key=True, index=True)
    street = Column(String, nullable=False)
    number = Column(String, nullable=False)
    city = Column(String, nullable=False)
    lat = Column(Float, default=0)
    long = Column(Float, default=0)
    user_id = Column(Integer, ForeignKey("users.id"))

    address_owner = relationship("User", back_populates="addresses")
