"""
Module for the User model
"""
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship

from ..database import Base
from .mixins import TimestampMixin


# pylint: disable=too-few-public-methods
class User(Base, TimestampMixin):
    """
    Defines the SQLAlchemy model for 'users' table
    """

    __tablename__ = "users"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, index=True)
    email = Column(String, index=True, unique=True, nullable=False)
    phone = Column(String, nullable=False, unique=True)
    cpf = Column(String, nullable=False, unique=True)
    hashed_password = Column(String, nullable=False)
    push_token = Column(String, nullable=True)

    asked_helps = relationship("HelpRequest", back_populates="asking_user")
    addresses = relationship("Address", back_populates="address_owner")
    offered_helps = relationship("HelpOffer", back_populates="offering_user")
