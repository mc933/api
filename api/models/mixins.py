"""
Mixins offers a common feature set to other SQLAlchemy models
"""
from sqlalchemy import Column, DateTime
from sqlalchemy.sql import func


class TimestampMixin:
    """
    Mixin that offers timestamp fields for creation and update events
    """

    # pylint: disable=too-few-public-methods
    created_at = Column(DateTime(timezone=True), server_default=func.now())
    updated_at = Column(DateTime(timezone=True), onupdate=func.now())
