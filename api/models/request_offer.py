"""
Help Request - Offer pivot table
"""
from sqlalchemy import (
    Column,
    ForeignKey,
    Integer,
    PrimaryKeyConstraint,
)

from ..database import Base


# pylint: disable=too-few-public-methods
class RequestOffer(Base):
    """
    Defines the SQLAlchemy model for a Help Request
    """

    __tablename__ = "requests_offers"
    __table_args__ = (PrimaryKeyConstraint("help_request_id", "help_offer_id"),)
    help_request_id = Column(Integer, ForeignKey("help_requests.id"))
    help_offer_id = Column(Integer, ForeignKey("help_offers.id"))
