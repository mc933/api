"""
Help Item SQL Model
"""
from sqlalchemy import Column, Float, Integer, Numeric, Text
from sqlalchemy.orm import relationship
from sqlalchemy.sql.schema import ForeignKey

from ..database import Base
from .mixins import TimestampMixin

# pylint: disable=too-few-public-methods
class Item(Base, TimestampMixin):
    """
    A Help Request's item.
    """

    __tablename__ = "items"
    id = Column(Integer, primary_key=True, index=True)
    name = Column(Text)
    quantity = Column(Float)
    max_cost = Column(Numeric(5, 2), nullable=False)
    help_request_id = Column(Integer, ForeignKey("help_requests.id"))

    help_request = relationship("HelpRequest", back_populates="items")
