"""
Pydantic schema for an Help Request
"""
from typing import List
from pydantic import BaseModel  # pylint: disable=no-name-in-module

# pylint: disable=too-few-public-methods


class HelpOfferBase(BaseModel):
    """
    Base schema for a Help Offer
    """

    max_cost: float = 0
    notes: str = ""
    lat: float = 0
    long: float = 0
    help_day: str
    hour: str = ""
    type: str = ""


class HelpOfferCreate(HelpOfferBase):
    """
    Pydantic model for a Help Offer creation
    """


class HelpOffer(HelpOfferBase):
    """
    Pydantic schema for a help offer
    """

    id: int
    offering_user_id: int
    requests: List = []
    finished: bool

    class Config:
        """
        Sets up ORM integration
        """

        orm_mode = True
