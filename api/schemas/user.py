"""
Pydantic schema for an user
"""
import re
from datetime import datetime
from typing import List

from pydantic import BaseModel, validator  # pylint: disable=no-name-in-module

from .address import Address
from .help_offer import HelpOffer
from .help_request import HelpRequest


# pylint: disable=too-few-public-methods
class UserBase(BaseModel):
    """
    Base Pydantic model for an User
    """

    name: str
    email: str
    phone: str
    cpf: str
    push_token: str = ""

    # pylint: disable=invalid-name,no-self-argument,no-self-use
    @validator("cpf")
    def get_only_digits(cls, v):
        """
        Returns only the CPF digits
        """
        digits = re.findall(r"\d", v)
        return "".join(digits)


class UserCreate(UserBase):
    """
    Fields that are needed while creating an user
    that are not already defined in UserBase
    """

    password: str


class User(UserBase):
    """
    Fields that are needed while visualizing an user
    that are not already defined in UserBase
    """

    id: int
    addresses: List[Address] = None
    asked_helps: List[HelpRequest] = []
    offered_helps: List[HelpOffer] = []
    created_at: datetime

    class Config:
        """
        Sets ORM Mode
        """

        orm_mode = True


class UserPublic(BaseModel):
    """
    Public User Data
    """

    id: int
    name: str
    email: str
    phone: str
    push_token: str = ""

    class Config:
        """
        Sets ORM Mode
        """

        orm_mode = True
