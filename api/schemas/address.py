"""
Pydantic schema for an Address
"""

from pydantic import BaseModel  # pylint: disable=no-name-in-module

# pylint: disable=too-few-public-methods


class AddressBase(BaseModel):
    """
    Base schema for an address
    """

    street: str
    number: str
    city: str
    lat: float = 0
    long: float = 0


class AddressCreate(AddressBase):
    """
    Pydantic model for an address creation
    """


class Address(AddressBase):
    """
    Pydantic schema for an address
    """

    id: int

    class Config:
        """
        Sets up ORM integration
        """

        orm_mode = True
