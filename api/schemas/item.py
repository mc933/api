"""
Pydantic schema for a Help Request's Item
"""
from pydantic import BaseModel  # pylint: disable=no-name-in-module


# pylint: disable=too-few-public-methods


class ItemBase(BaseModel):
    """
    Base schema for a Help Request Item
    """

    name: str
    quantity: float
    max_cost: float


class ItemCreate(ItemBase):
    """
    Pydantic model for a Help Request creation
    """


class Item(ItemBase):
    """
    Pydantic schema for a help request
    """

    id: int

    class Config:
        """
        Sets up ORM integration
        """

        orm_mode = True
