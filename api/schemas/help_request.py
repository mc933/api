"""
Pydantic schema for an Help Request
"""
from typing import List
from pydantic import BaseModel  # pylint: disable=no-name-in-module

from .address import Address

# pylint: disable=too-few-public-methods


class HelpRequestBase(BaseModel):
    """
    Base schema for a Help Request
    """

    max_cost: float
    notes: str = ""
    help_day: str
    min_available_hour: str = ""
    max_available_hour: str = ""
    type: str = ""
    items: List = []
    address_id: int


class HelpRequestCreate(HelpRequestBase):
    """
    Pydantic model for a Help Request creation
    """


class HelpRequest(HelpRequestBase):
    """
    Pydantic schema for a help request
    """

    id: int
    asking_user_id: int
    offers: List = []
    fulfilled: bool
    address: Address = None

    class Config:
        """
        Sets up ORM integration
        """

        orm_mode = True


class HelpRequestPlain(HelpRequestBase):
    """
    Pydantic schema for a help request
    """

    id: int
    asking_user_id: int
    fulfilled: bool

    class Config:
        """
        Sets up ORM integration
        """

        orm_mode = True
