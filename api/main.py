"""
Here be awesome code!
"""

import uvicorn
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from .database import Base, engine
from .routers import address, auth, help_offer, help_request, user

Base.metadata.create_all(bind=engine)

app = FastAPI(debug=True)

origins = [
    "http://localhost",
    "http://localhost:8080",
    "http://esther.felipevr.com",
    "https://esther.felipevr.com",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(address.router)
app.include_router(auth.router)
app.include_router(help_offer.router)
app.include_router(help_request.router)
app.include_router(user.router)


@app.get("/")
def home():
    """
    The root of the API
    """
    return "Hello, world!"


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
