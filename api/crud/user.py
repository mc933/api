"""
Crud for Users
"""
from sqlalchemy.orm import Session

from ..auth import get_password_hash
from ..models import User
from ..schemas import UserCreate


def get_user_by_email_or_phone(
    db: Session, email: str = None, phone: str = None
) -> User:
    """
    Finds an User through its email
    """
    return db.query(User).filter((User.email == email) | (User.phone == phone)).first()


def get_user(db: Session, user_id: int):
    """
    Gets a single user by ID
    """
    return db.query(User).get(user_id)


def create_user(db: Session, user: UserCreate) -> User:
    """
    Creates a new user in the database
    """
    hashed_password = get_password_hash(user.password)
    db_user = User(**user.dict(exclude={"password"}), hashed_password=hashed_password)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def update_user(db: Session, user_id: int, user_data: UserCreate) -> User:
    """
    Updates an user
    """
    hashed_password = get_password_hash(user_data.password)
    data = user_data.dict(exclude={"id", "password"})
    data["hashed_password"] = hashed_password
    db.query(User).filter(User.id == user_id).update(data)
    db.commit()

    return db.query(User).get(user_id)
