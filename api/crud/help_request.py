"""
Crud for Help Requests
"""
from typing import List
from sqlalchemy.orm import Session, noload


from .help_offer import create_help_offer

from ..models import HelpRequest, User, Item
from ..schemas import HelpOfferCreate, HelpRequestCreate, ItemCreate


def get_help_requests(db: Session, skip: int = 0, limit: int = 100):
    """
    Returns all the registered Help Requests
    """
    return (
        db.query(HelpRequest)
        .order_by(HelpRequest.fulfilled.asc())
        .offset(skip)
        .limit(limit)
        .all()
    )


def get_help_request(db: Session, id_: int, exclude_offers=False) -> HelpRequest:
    """
    Returns a single help request by id
    """
    # options(db.defer('location')
    query = db.query(HelpRequest)

    if exclude_offers:
        query.options(noload("offers"))

    return query.filter(HelpRequest.id == id_).first()


def get_help_request_item(db: Session, id_: int) -> Item:
    """
    Returns all the registered Help Requests
    """
    return db.query(Item).filter(Item.id == id_).first()


def create_help_request(
    db: Session, help_request: HelpRequestCreate, user: User
) -> HelpRequest:
    """
    Creates a new help request
    """
    items = [ItemCreate(**item) for item in help_request.items]
    db_help_request = HelpRequest(
        **help_request.dict(exclude={"items"}), asking_user_id=user.id
    )

    db.add(db_help_request)
    db.commit()
    db.refresh(db_help_request)
    db_help_request = create_items(db, db_help_request, items)

    return db_help_request


def offer_help_to_request(
    db: Session,
    help_request: HelpRequest,
    help_offer: HelpOfferCreate,
    offering_user: User,
):
    """
    Creates a new HelpOffer associated with the Offering User and attaches
    it to an already created Help Request.
    """
    if help_request.offers:
        raise Exception("Request already has offer")

    help_offer = create_help_offer(db, help_offer, offering_user)
    help_request.offers.append(help_offer)
    db.add(help_request)
    db.commit()
    db.refresh(help_request)

    return help_request


def create_items(db: Session, help_request: HelpRequest, items: List[ItemCreate]):
    """
    Insert items into the database and appends them to a help_request
    """
    items = [Item(help_request_id=help_request.id, **item.dict()) for item in items]
    db.bulk_save_objects(items)
    db.commit()
    db.refresh(help_request)

    return help_request
