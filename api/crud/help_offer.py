"""
Crud for Help Offers
"""
from sqlalchemy.orm import Session

from ..models import HelpOffer, User
from ..schemas import HelpOfferCreate, HelpRequestCreate


def get_help_offers(db: Session, skip: int = 0, limit: int = 100):
    """
    Returns all the registered Help Offers
    """
    return (
        db.query(HelpOffer)
        .order_by(HelpOffer.finished.asc())
        .offset(skip)
        .limit(limit)
        .all()
    )


def get_help_offer(db: Session, id_: int) -> HelpOffer:
    """
    Returns all the registered Help Offers
    """
    return db.query(HelpOffer).filter(HelpOffer.id == id_).first()


def create_help_offer(
    db: Session, help_offer: HelpOfferCreate, user: User
) -> HelpOffer:
    """
    Creates a new help request
    """
    db_help_offer = HelpOffer(**help_offer.dict(), offering_user_id=user.id)

    db.add(db_help_offer)
    db.commit()
    db.refresh(db_help_offer)

    return db_help_offer


def accept_help_offer(
    db: Session,
    help_offer: HelpOffer,
    help_request: HelpRequestCreate,
    offering_user: User,
):
    """
    Creates a new Help Request associated with a Help Offer
    """

    # pylint: disable=import-outside-toplevel
    from ..crud.help_request import create_help_request

    help_request = create_help_request(db, help_request, offering_user)
    help_offer.requests.append(help_request)
    db.add(help_offer)
    db.commit()
    db.refresh(help_offer)

    return help_offer
