"""
Crud for Addresses
"""
from sqlalchemy.orm import Session

from ..models import Address, User
from ..schemas import AddressCreate


def create_address(db: Session, address: AddressCreate, user: User):
    """
    Creates a new address
    """

    db_address = Address(**address.dict(), user_id=user.id)

    db.add(db_address)
    db.commit()
    db.refresh(db_address)

    return db_address


def get_addresses(db: Session, skip: int = 0, limit: int = 100):
    """
    Lists all addresses
    """
    return db.query(Address).offset(skip).limit(limit).all()


def get_address(db: Session, address_id: int):
    """
    Gets a single user by address
    """
    return db.query(Address).get(address_id)
